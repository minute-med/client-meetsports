var Services = angular.module('starter.services', ['ngResource']);

Services.factory('Events', function($resource, $stateParams) {
  // Might use a resource here that returns a JSON array
  return $resource('http://localhost:4242/api/events/:id', null, {
    query : {method : 'GET', isArray : false},
    get : {method : 'GET', url: 'http://localhost:4242/api/events/' + $stateParams.id},
    put : {method : 'PUT'}
  });
});

Services.factory('AuthRessource', function($resource) {
  // Might use a resource here that returns a JSON array
  return $resource('http://localhost:4242/api/auth', null, {

    login : {method : 'POST', url : 'http://localhost:4242/api/auth/sign-in'},
    register : {method : 'POST', url : 'http://localhost:4242/api/auth/sign-up'},
    query : {method : 'GET', isArray : false}
  });

});

Services.factory('Auth', function() {
 return {
  user: function() {
    return JSON.parse(window.localStorage['_user']);
  }
}
});

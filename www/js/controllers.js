angular.module('starter.controllers', [])


.controller('SignUpCtrl',['$scope','$location','$http', 'AuthRessource', function($scope, $location, $http, AuthRessource) {

  $scope.user = {};

  $scope.submit = function(user){

    console.log(user);
    AuthRessource.register(user, function(res){
      if(!res.success) {alert('error');console.log(res);return false;}
      // insert token in local storage
      window.localStorage['jwt_token'] = res.token;
      window.localStorage['_user'] = JSON.stringify(res.user);


      $http.defaults.headers.common['Authorization'] = res.token;
      $location.path('/menu');
    });
  }

}])

.controller('SignInCtrl',['$scope', '$location','$http', 'AuthRessource', function($scope, $location, $http, AuthRessource) {

  $scope.user = {};

  $scope.submit = function(user){

    AuthRessource.login(user, function(response){

      if(!response.success){alert('error');console.log(err);}

      window.localStorage['jwt_token'] = response.token;
      window.localStorage['_user'] = JSON.stringify(response.user);

      $http.defaults.headers.common['Authorization'] = response.token;

      console.log(response.token);

      $location.path('/menu');

    });
  };

}])

.controller('MyEventsCtrl',['$scope','$location','$stateParams', 'Events', function($scope, $location, $stateParams, Events) {

  $scope.events = [];

  $scope.delete = function(evt){
    console.log('delete event : ' + evt.title);


  Events.delete({id: $stateParams.id},function(response){
    console.log(response);
  });

  }


}])



.controller('MyEventsDetailCtrl',['$scope','$location','$stateParams', 'Events', function($scope, $location, $stateParams, Events) {

  $scope.evt = {};

  $scope.submit = function(evt){
    console.log('edit event : ' + evt.title);


  Events.put({id: $stateParams.id}, $scope.evt, function(response){
    console.log(response);
  });

  }


}])


.controller('EventListCtrl',['$scope','$location', '$stateParams', '$ionicListDelegate','Auth', 'Events',

 function($scope, $location, $stateParams, $ionicListDelegate, Auth, Events) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

$scope.events = [];

var e = Events.query(
    // success acllback
    function(events, resheader){
      $scope.events = events['events'];
    },
  // error callback
  function(resheader){
    if(resheader.status == 401)
      $location.path('/auth');
    else if(resheader.status == 403)
      alert('unautorized');
  });


$scope.join = function(evt){


  var user = Auth.user() || false;

  if(user) {
    evt.participants.push(user);
    Events.put(evt, function(response){
      console.log('res :');
      console.log(response);

      $ionicListDelegate.closeOptionButtons();
      // toggle affichage bouton join / leave
    });

  }
  else
    alert("can't get logedin user");

  console.log('try join evt : ' + evt.title);




};

}])

.controller('EventDetailCtrl',['$scope','$location', 'Events', function($scope, $location, Events) {

  $scope.evt = {};
  Events.get(function(res){
    if(res.success)
      $scope.evt = res['event'] || {};
  });

}])

.controller('CreateEventCtrl', ['$scope','$location', 'Auth', 'Events', function($scope, $location, Auth, Events) {


  $scope.Event = {};

  $scope.submit = function(evt){

    evt._ownerid = Auth.user()._id;

    Events.save(evt, function(err, evt){
      if(err) console.log(err);
      $location.path('/event-list');
      // redirection vers "mes evenements"
    });

  };

}]);

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
  if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    cordova.plugins.Keyboard.disableScroll(true);

  }
  if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('authPage', {
    url: '/auth',
    templateUrl : 'templates/auth/auth.html'
  })

  .state('sign-up', {
    url: '/sign-up',
    templateUrl : 'templates/auth/sign-up.html',
    controller: 'SignUpCtrl'
  })

  .state('sign-in', {
    url: '/sign-in',
    templateUrl : 'templates/auth/sign-in.html',
    controller: 'SignInCtrl'
  })

  .state('menu', {
    url: '/',
    templateUrl: 'templates/menu.html'
  })

  .state('create-event', {
    url: '/create-event',
    templateUrl : 'templates/events/create_event.html',
    controller: 'CreateEventCtrl'
  })

  .state('my-events', {
    url: '/my-events',
    templateUrl : 'templates/events/my-events.html'
  })

  .state('edit-event', {
    url: '/edit-event',
    templateUrl : 'templates/events/my-events-edit.html'
  })

  .state('event-list', {
    url: '/event-list',
    templateUrl : 'templates/events/event-list.html',
    controller: 'EventListCtrl'
  })

  .state('event-detail', {
    url: '/event-detail/:id',
    templateUrl : 'templates/events/event-detail.html',
    controller: 'EventDetailCtrl'
  })  
  ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');

});
